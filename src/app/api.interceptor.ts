import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import * as fromApp from './store/app.reducers';
import * as fromAuth from './login/store/auth.reducers';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    constructor(private store: Store<fromApp.AppState>) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return this.store.select('auth').pipe(
            take(1),
            switchMap((authState: fromAuth.State) => {

                // Add the Authorization header in case token exists
                const token = authState.token;
                if (token) {
                    req = req.clone({headers: req.headers.append('Authorization', `Bearer ${token}`)});
                }
                // Setup the base API url
                req = req.clone({url: `${location.protocol}//${location.hostname}:3000/${req.url}`});

                return  next.handle(req);
            })
        );
    }
}
