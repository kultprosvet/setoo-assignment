import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';
import { LoginComponent } from './login/login.component';
import { ApiInterceptor } from './api.interceptor';
import { reducers } from './store/app.reducers';
import { AuthEffects } from './login/store/auth.effects';
import { FlightsEffects } from './flights-data/store/flights.effects';
import { FlightsDataModule } from './flights-data/flights-data.module';
import { AuthGuard } from './login/auth-guard.service';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
      AppComponent,
      LoginComponent
  ],
  imports: [
      BrowserModule,
      ReactiveFormsModule,
      RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload' }),
      HttpClientModule,
      FlightsDataModule,
      StoreModule.forRoot(reducers),
      EffectsModule.forRoot([AuthEffects, FlightsEffects]),
      !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [
      AuthGuard,
      { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
