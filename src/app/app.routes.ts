import { Routes } from '@angular/router';
import { FlightsDataComponent } from './flights-data/flights-data.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './login/auth-guard.service';

export const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'flight-data',
        component: FlightsDataComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {path: '**', redirectTo: '/pips', pathMatch: 'full'},
    {path: '*', redirectTo: '/pips', pathMatch: 'full'}
];
