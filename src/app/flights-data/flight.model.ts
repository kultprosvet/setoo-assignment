export class Flight {
    constructor(
        public departCity: string,
        public departAirportName: string,
        public departAirport: string,
        public flightDate: string,
        public flightTime: string,
        public landingDate: string,
        public landingTime: string,
        public destinationCity: string,
        public destinationAirport: string,
        public destinationAirportName: string,
        public flightNumber: number,
        public airlineId: string,
        public airlineName: string,
        public maxDelayInMinutes:  number,
        public departureDateTime:  number,
        public arrivalDateTime:  number,
        public cost:  number,
        public compensation:  number,
        public triggerTime: Array<number>
    ) {}
}
