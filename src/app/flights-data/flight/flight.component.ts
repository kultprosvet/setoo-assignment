import { Component, OnInit, Input} from '@angular/core';
import { Flight } from '../flight.model';

@Component({
    selector: '[app-flight]',
    templateUrl: './flight.component.html',
    styleUrls: ['./flight.component.scss']
})
export class FlightComponent implements OnInit {

    @Input() flight: Flight;
    @Input() prevFlight: Flight;

    constructor() {
    }

    ngOnInit() {
    }

}
