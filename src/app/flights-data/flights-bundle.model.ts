export class FlightsBundle {
    constructor(
        public departAirport: string,
        public destinationAirport: string,
        public departureDateTime: number,
    ) {}
}
