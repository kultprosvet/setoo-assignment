import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightsBundleComponent } from './flights-bundle.component';

describe('FlightsBundleComponent', () => {
  let component: FlightsBundleComponent;
  let fixture: ComponentFixture<FlightsBundleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightsBundleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightsBundleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
