import { Component, OnInit, Input} from '@angular/core';
import { faPlaneDeparture, faPlaneArrival } from '@fortawesome/free-solid-svg-icons';
import { Flights } from '../flights.model';

@Component({
    selector: '[app-flights-bundle]',
    templateUrl: './flights-bundle.component.html',
    styleUrls: ['./flights-bundle.component.scss']
})
export class FlightsBundleComponent implements OnInit {
    faPlaneDeparture = faPlaneDeparture;
    faPlaneArrival = faPlaneArrival;

    @Input() flightsType: string;
    @Input() flights: Flights;

    constructor() {

    }

    ngOnInit() {
    }

}
