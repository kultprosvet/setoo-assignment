import { Component, OnInit} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromApp from '../store/app.reducers';
import * as fromAuth from '../login/store/auth.reducers';
import * as AuthActions from '../login/store/auth.actions';
import * as fromFlights from './store/flights.reducers';
import * as FlightsActions from './store/flights.actions';

@Component({
    selector: 'app-flights-data',
    templateUrl: './flights-data.component.html',
    styleUrls: ['./flights-data.component.scss']
})
export class FlightsDataComponent implements OnInit {
    authState: Observable<fromAuth.State>;
    flightsData: Observable<fromFlights.State>;

    constructor(private store: Store<fromApp.AppState>) {}

    ngOnInit() {
        this.authState = this.store.select('auth');
        this.flightsData = this.store.select('flights');
        this.store.dispatch(new FlightsActions.GetFlights());
    }

    onLogout() {
        this.store.dispatch(new AuthActions.Logout());
    }
}
