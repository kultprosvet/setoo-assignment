import { FlightsDataModule } from './flights-data.module';

describe('FlightsDataModule', () => {
  let flightsDataModule: FlightsDataModule;

  beforeEach(() => {
    flightsDataModule = new FlightsDataModule();
  });

  it('should create an instance', () => {
    expect(flightsDataModule).toBeTruthy();
  });
});
