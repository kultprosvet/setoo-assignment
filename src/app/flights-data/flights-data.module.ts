import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { FlightsDataComponent } from './flights-data.component';
import { FlightComponent } from './flight/flight.component';
import { FlightsBundleComponent } from './flights-bundle/flights-bundle.component';
import { MainPipe } from '../pipes/main-pipe.module';


@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    MainPipe
  ],
  declarations: [
      FlightsDataComponent,
      FlightComponent,
      FlightsBundleComponent
  ]
})
export class FlightsDataModule { }
