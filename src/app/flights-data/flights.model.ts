import { Flight } from './flight.model';
import { FlightsBundle } from './flights-bundle.model';

export class Flights {
    constructor(
        public outboundFlights: Flight[],
        public inboundFlights: Flight[],
        public outboundFlightsInfo: FlightsBundle[],
        public inboundFlightsInfo: FlightsBundle[],
        public price: number,
        public compensation: number,
        public pipTemplate: string,
        public distributorName: string,
        public currency: string,
        public country: string,
        public language: string,
        public proposalIds: Array<string>,
        public landingPageData: {
                title: string,
                subTitle: string,
                content: string
            }
    ) {}
}
