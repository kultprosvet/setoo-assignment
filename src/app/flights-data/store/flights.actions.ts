import { Action } from '@ngrx/store';

import { Flights } from '../flights.model';

export const GET_FLIGHTS = 'GET_FLIGHTS';
export const ADD_FLIGHTS = 'ADD_FLIGHTS';

export class GetFlights implements Action {
    readonly type = GET_FLIGHTS;
}

export class AddFlights implements Action {
    readonly type = ADD_FLIGHTS;

    constructor (public payload: Flights ) {}
}

export type FlightsActions = GetFlights | AddFlights;
