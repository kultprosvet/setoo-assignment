import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap, mergeMap} from 'rxjs/operators';
import { from } from 'rxjs';

import * as FlightsActions from './flights.actions';

@Injectable()
export class FlightsEffects {

    @Effect()
    getFlights = this.actions$
        .pipe(
            ofType(FlightsActions.GET_FLIGHTS),
            switchMap(() => {
                return from(this.httpClient.get('flightData'));
            }),
            mergeMap((flightsData: any) => {
                flightsData.outboundFlightsInfo = {};
                flightsData.outboundFlightsInfo.departureDateTime = flightsData.outboundFlights[0]['departureDateTime'];
                flightsData.outboundFlightsInfo.departAirport = flightsData.outboundFlights[0]['departAirport'];
                flightsData.outboundFlightsInfo.destinationAirport = flightsData.outboundFlights[flightsData.outboundFlights.length - 1]['destinationAirport'];

                flightsData.inboundFlightsInfo = {};
                flightsData.inboundFlightsInfo.departureDateTime = flightsData.inboundFlights[0]['departureDateTime'];
                flightsData.inboundFlightsInfo.departAirport = flightsData.inboundFlights[0]['departAirport'];
                flightsData.inboundFlightsInfo.destinationAirport = flightsData.inboundFlights[flightsData.inboundFlights.length - 1]['destinationAirport'];

                return [
                    {
                        type: FlightsActions.ADD_FLIGHTS,
                        payload: flightsData
                    }
                ];
            })
        );

    constructor(private httpClient: HttpClient,
                private actions$: Actions) {
    }
}
