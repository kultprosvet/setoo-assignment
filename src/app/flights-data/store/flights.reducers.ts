import * as FlightsActions from './flights.actions';
import { Flights } from '../flights.model';

export interface State {
    flights: Flights;
}

const initialState: State = {
    flights: null
};

export function flightsReducer(state = initialState, action: FlightsActions.FlightsActions) {
    switch (action.type) {
        case FlightsActions.ADD_FLIGHTS:
            return {
                ...state,
                flights: action.payload
            };
        default:
            return state;
    }
}
