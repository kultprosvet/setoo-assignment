import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as AuthActions from './store/auth.actions';
import * as fromApp from '../store/app.reducers';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public loginForm: FormGroup;
    public errorMessages: Array<string>;

    constructor(
        private formBuilder: FormBuilder,
        private store: Store<fromApp.AppState> ) {
    }

    ngOnInit() {
        this.errorMessages = [];
        this.loginForm = this.formBuilder.group({
            username: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
    }

    login(formDetails: any) {
        const loginDetails: any = formDetails.value;
        this.errorMessages = [];
        if (this.loginForm.controls.username.invalid) {
            this.errorMessages.push('Error: Valid Email is mandatory');
        }
        if (this.loginForm.controls.password.invalid) {
            this.errorMessages.push('Error: Password is mandatory');
        }
        if (this.loginForm.invalid) {
            return;
        }
        const loginFormDetails: any = {username: loginDetails.username, password: loginDetails.password};
        this.errorMessages = [];

        this.store.dispatch(new AuthActions.TrySignup(loginFormDetails));
    }
}
