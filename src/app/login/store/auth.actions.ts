import { Action } from '@ngrx/store';

export const TRY_SIGNUP = 'TRY_SIGNUP';
export const SET_TOKEN = 'SET_TOKEN';
export const LOGOUT = 'LOGOUT';

export class TrySignup implements Action {
    readonly type = TRY_SIGNUP;

    constructor(public payload: {username: string, password: string}) {}
}

export class SetToken implements Action {
    readonly type = SET_TOKEN;

    constructor (public payload: string ) {}
}

export class Logout implements Action {
    readonly type = LOGOUT;
}

export type AuthActions = TrySignup | SetToken | Logout;
