import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap, mergeMap} from 'rxjs/operators';
import { from } from 'rxjs';

import * as AuthActions from './auth.actions';

@Injectable()
export class AuthEffects {
    @Effect()
    authLogin = this.actions$
        .pipe(
            ofType(AuthActions.TRY_SIGNUP),
            map((action: AuthActions.TrySignup) => {
                return action.payload;
            }),
            switchMap((authData: { username: string, password: string }) => {
                return from(this.httpClient.get('login', {params: authData}));
            }),
            mergeMap((authResponse: { token: string }) => {
                this.router.navigate(['/flight-data']);
                return [
                    {
                        type: AuthActions.SET_TOKEN,
                        payload: authResponse.token
                    }
                ];
            })
        );

    @Effect({dispatch: false})
    authLogout = this.actions$
        .pipe(
            ofType(AuthActions.LOGOUT),
            tap(() => {
                this.router.navigate(['/']);
            })
        );

    constructor(private httpClient: HttpClient,
                private actions$: Actions,
                private router: Router) {
    }
}
