import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecondsToDhmsPipe } from './seconds-to-hms.pipe';

@NgModule({
    declarations: [ SecondsToDhmsPipe ],
    imports: [ CommonModule ],
    exports: [ SecondsToDhmsPipe ]
})

export class MainPipe {}
