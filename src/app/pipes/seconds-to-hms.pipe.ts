import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'SecondsToDhms'
})

export class SecondsToDhmsPipe implements PipeTransform {
    transform(seconds: any) {
        seconds = Number(seconds);
        const d = Math.floor(seconds / (3600 * 24));
        seconds  -= d * 3600 * 24;
        const h = Math.floor(seconds / 3600);
        const m = Math.floor(seconds % 3600 / 60);
        const s = Math.floor(seconds % 3600 % 60);

        const dDisplay = d > 0 ? d + (d === 1 ? ' day, ' : 'd ') : '';
        const hDisplay = h > 0 ? h + (h === 1 ? ' hour, ' : 'h ') : '';
        const mDisplay = m > 0 ? m + (m === 1 ? ' minute, ' : 'm ') : '';
        const sDisplay = s > 0 ? s + (s === 1 ? ' second' : '') : '';
        return dDisplay + hDisplay + mDisplay;
    }
}
