import {ActionReducerMap} from '@ngrx/store';

import * as fromAuth from '../login/store/auth.reducers';
import * as fromFlights from '../flights-data/store/flights.reducers';

export interface AppState {
    auth: fromAuth.State;
    flights: fromFlights.State;
}

export const reducers: ActionReducerMap<AppState> = {
    auth: fromAuth.authReducer,
    flights: fromFlights.flightsReducer
}
